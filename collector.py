#!/usr/bin/python3
import os, getopt, sys

def getIndent(line):
    indent = 0
    while line[indent:indent+1] == '\t':
        indent += 1
    return indent

def genTuple(filename, subFilename, profiles, value = ''):
    return {'filename': '%s: %s' % (filename, subFilename), 'profile': ' '.join(profiles), 'value': value}

onlyDuplicates = False
printTable = False
withAbstractions = 2 # 0: without abstractions, 1: with abstractions, 2: don't care

try:
    optList, args = getopt.getopt(sys.argv[1:], 'dta:')
except:
    print(str(err))
    print('arg error')
    exit(1)
for opt, arg in optList:
    if opt == '-d':
        onlyDuplicates = True # print only row's that have been called more than one
    if opt == '-t':
        printTable = True # print table count
    if opt == '-a':
        withAbstractions = int(arg) # print only row's that have been included in an abstraction

includes = dict()
capability = dict()
mount = dict()
umount = dict()
network = dict()
rules = dict()
changeProfile = dict()
tunables = dict()

for filename in os.listdir('.'):
    fo = open(filename, 'r')
    subFilename = ''
    profiles = []
    previousIndent = 0
    lineNum = 0

    for line in fo:
        lineNum += 1
        indent = getIndent(line)
        if indent == previousIndent - 1:
            profiles.pop()

        splittedLine = line.split()
        if subFilename == '':
            # new subfile
            subFilename = line.strip()
        elif len(splittedLine) == 0:
            # newline
            subFilename = ''
            profiles = []
        elif splittedLine[len(splittedLine) - 1][-1:] == '{':
            # profile
            profiles.append(line.strip())
        elif splittedLine[0] in ['include', '#include']:
            # include
            cap = splittedLine[1] if len(splittedLine) > 1 else ''
            if splittedLine[1] not in includes:
                includes[splittedLine[1]] = [genTuple(filename, subFilename, profiles)]
            else:
                includes[splittedLine[1]].append(genTuple(filename, subFilename, profiles))
            pass
        elif splittedLine[0][0:1] in ["/", "@"] and len(splittedLine) == 2:
            # rules
            if splittedLine[0] not in rules:
                rules[splittedLine[0]] = [genTuple(filename, subFilename, profiles, splittedLine[1])]
            else:
                rules[splittedLine[0]].append(genTuple(filename, subFilename, profiles, splittedLine[1]))
        elif splittedLine[0][0:1] in ['@'] and len(splittedLine) == 1:
            # tunables
            if splittedLine[0] not in tunables:
                tunables[splittedLine[0]] = [genTuple(filename, subFilename, profiles)]
            else:
                tunables[splittedLine[0]].append(genTuple(filename, subFilename, profile))
        elif splittedLine[0] in ["capability", "capability,"]:
            # capability
            cap = '' if len(splittedLine) < 2 else splittedLine[1]
            if cap not in capability:
                capability[cap] = [genTuple(filename, subFilename, profiles)]
            else:
                capability[cap].append(genTuple(filename, subFilename, profiles))
        elif splittedLine[0] in ["network", "network,"]:
            # network
            net = '' if len(splittedLine) < 2 else ' '.join(splittedLine[1:])
            if net not in network:
                network[net] = [genTuple(filename, subFilename, profiles)]
            else:
                network[net].append(genTuple(filename, subFilename, profiles))
        elif splittedLine[0] in ["change_profile"]:
            # change_profile
            chan = ' '.join(splittedLine[1:])
            if chan not in changeProfile:
                changeProfile[chan] = [genTuple(filename, subFilename, profiles)]
            else:
                changeProfile[chan].append(genTuple(filename, subFilename, profiles))

        elif splittedLine[0] in ["mount", "mount,"]:
            # mount
            mon = '' if len(splittedLine) < 2 else ' '.join(splittedLine[1:])
            if mon not in mount:
                mount[mon] = [genTuple(filename, subFilename, profiles)]
            else:
                mount[mon].append(genTuple(filename, subFilename, profiles))
        elif splittedLine[0] in ["umount", "umount,"]:
            # mount
            umon = '' if len(splittedLine) < 2 else ' '.join(splittedLine[1:])
            if umon not in umount:
                umount[mon] = [genTuple(filename, subFilename, profiles)]
            else:
                umount[mon].append(genTuple(filename, subFilename, profiles))

        else:
            print('error in %s:%d: %s' % (filename, lineNum, line))
            exit(1)

        previousIndent = indent

for key, value in sorted(includes.items()):
    if not onlyDuplicates or len(value) > 1:
        if printTable:
            hasAbstractions = False
            for profile in value:
                if 'abstractions' in profile['filename']:
                    hasAbstractions = True
                    break
            if withAbstractions == 2 or withAbstractions == 1 and hasAbstractions or withAbstractions == 0 and not hasAbstractions:
                print('include %s\t%d' % (key, len(value)))
        else:
            print('include', key)
            for profile in value:
                print('\tinclude %s\t%s:\t%s' % (key, profile['filename'], profile['profile']), sep = '')
for key, value in sorted(capability.items()):
    if not onlyDuplicates or len(value) > 1:
        if printTable:
            hasAbstractions = False
            for profile in value:
                if 'abstractions' in profile['filename']:
                    hasAbstractions = True
                    break
            if withAbstractions == 2 or withAbstractions == 1 and hasAbstractions or withAbstractions == 0 and not hasAbstractions:
                print('capability %s\t%d' % (key, len(value)))
        else:
            print('capability', key)
            for profile in value:
                print('\tcapability %s\t%s:\t%s' % (key, profile['filename'], profile['profile']), sep = '')
for key, value in sorted(network.items()):
    if not onlyDuplicates or len(value) > 1:
        if printTable:
            hasAbstractions = False
            for profile in value:
                if 'abstractions' in profile['filename']:
                    hasAbstractions = True
                    break
            if withAbstractions == 2 or withAbstractions == 1 and hasAbstractions or withAbstractions == 0 and not hasAbstractions:
                print('network %s\t%d' % (key, len(value)))
        else:
            print('network', key)
            for profile in value:
                print('\tnetwork %s\t%s:\t%s' % (key, profile['filename'], profile['profile']), sep = '')
for key, value in sorted(rules.items()):
    if not onlyDuplicates or len(value) > 1:
        if printTable:
            hasAbstractions = False
            for profile in value:
                if 'abstractions' in profile['filename']:
                    hasAbstractions = True
                    break
            if withAbstractions == 2 or withAbstractions == 1 and hasAbstractions or withAbstractions == 0 and not hasAbstractions:
                print('%s\t%d' % (key, len(value)))
        else:
            print(key)
            for profile in value:
                print('\t%s\t%s:\t%s\t%s' % (key, profile['filename'], profile['profile'], profile['value']), sep = '')
for key, value in sorted(mount.items()):
    if not onlyDuplicates or len(value) > 1:
        if printTable:
            hasAbstractions = False
            for profile in value:
                if 'abstractions' in profile['filename']:
                    hasAbstractions = True
                    break
            if withAbstractions == 2 or withAbstractions == 1 and hasAbstractions or withAbstractions == 0 and not hasAbstractions:
                print('mount %s\t%d' % (key, len(value)))
        else:
            print('mount', key)
            for profile in value:
                print('\tmount %s\t%s:\t%s' % (key, profile['filename'], profile['profile']), sep = '')
for key, value in sorted(umount.items()):
    if not onlyDuplicates or len(value) > 1:
        if printTable:
            hasAbstractions = False
            for profile in value:
                if 'abstractions' in profile['filename']:
                    hasAbstractions = True
                    break
            if withAbstractions == 2 or withAbstractions == 1 and hasAbstractions or withAbstractions == 0 and not hasAbstractions:
                print('umount %s\t%d' % (key, len(value)))
        else:
            print('umount', key)
            for profile in value:
                print('\tumount %s\t%s:\t%s' % (key, profile['filename'], profile['profile']), sep = '')
for key, value in sorted(changeProfile.items()):
    if not onlyDuplicates or len(value) > 1:
        if printTable:
            hasAbstractions = False
            for profile in value:
                if 'abstractions' in profile['filename']:
                    hasAbstractions = True
                    break
            if withAbstractions == 2 or withAbstractions == 1 and hasAbstractions or withAbstractions == 0 and not hasAbstractions:
                print('change_profile %s\t%d' % (key, len(value)))
        else:
            print('change_profile', key)
            for profile in value:
                print('\tchange_profile %s\t%s:\t%s' % (key, profile['filename'], profile['profile']), sep = '')
for key, value in sorted(tunables.items()):
    if not onlyDuplicates or len(value) > 1:
        if printTable:
            hasAbstractions = False
            for profile in value:
                if 'abstractions' in profile['filename']:
                    hasAbstractions = True
                    break
            if withAbstractions == 2 or withAbstractions == 1 and hasAbstractions or withAbstractions == 0 and not hasAbstractions:
                print('tunables %s\t%d' % (key, len(value)))
        else:
            print('tunables', key)
            for profile in value:
                print('\ttunables %s\t%s:\t%s' % (key, profile['filename'], profile['profile']), sep = '')
