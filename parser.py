#!/usr/bin/python3

import sys, string

def getLenExcludingComment(splittedLine):
    for i in range(1, len(splittedLine)):
        if splittedLine[i][0:1] == '#':
            return i
    return len(splittedLine)

def isValid(splittedLine, indent):
    if splittedLine[0] in ["#include", "include"] and getLenExcludingComment(splittedLine) == 2:
        # an include
        print(indent, end = '')
        return True
    elif splittedLine[0][0:1] in ["/", "@"] and splittedLine[-1][-1:] != "{" and getLenExcludingComment(splittedLine) == 2:
        # a rule
        return True
    elif splittedLine[0][0:1] in ['@']:
        #tunables
        return True
    elif splittedLine[0] in ["capability", "capability,"] and getLenExcludingComment(splittedLine) <= 2:
        # capability
        return True
    elif splittedLine[0] in ["network", "network,"] and getLenExcludingComment(splittedLine) <= 3:
        # network
        return True
    elif splittedLine[0] in ["change_profile"] and getLenExcludingComment(splittedLine) <= 3:
        # change_profile
        return True
    elif splittedLine[0] in ["mount", "umount", "mount,", "umount,"] and getLenExcludingComment(splittedLine) <= 3:
        # mount / umount
        return True
    else:
        return False

fo = open(sys.argv[1], "r")
print(fo.name)
state = 0
indent = ""
lineNum = 0

for line in fo:
    lineNum += 1
    splittedLine = line.split()
    if getLenExcludingComment(splittedLine) > 0:
        if splittedLine[0][0:1] not in ["#", "}"]:
            print(indent, end = '')
        if isValid(splittedLine, indent):
            print(splittedLine[0], end = '')
            for i in range(1, getLenExcludingComment(splittedLine)):
                if i == getLenExcludingComment(splittedLine) - 1:
                    print("\t", splittedLine[i] if splittedLine[i][-1:] != ',' else splittedLine[i][:-1], sep = '', end = '')
                else:
                    print("\t", splittedLine[i], sep = '', end = '')
            print()
        elif splittedLine[0][0:1] == "#":
            continue
        elif splittedLine[0][0:1] in ["/", "^"] and splittedLine[getLenExcludingComment(splittedLine) - 1][-1:] == "{":
            # print(splittedLine[0])
            print(line.strip())
            state += 1
            indent += "\t"
        elif splittedLine[0] == "}":
            state -= 1
            indent = indent[0:-1]
        else:
            print("error in line ", lineNum, ": ", line, sep = '')
            exit(1)
print()
fo.close

