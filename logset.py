#!/usr/bin/env python
# -*- coding: utf-8 -*-
import subprocess

API_SEARCH = '/synosrc/curr/ds.base/lnxscripts/tool/api_search.py'
SYNOLogSet1 = 'SYNOLogSet1'
SLIBLogSet = 'SLIBLogSetByVA'
PLATFORM = 'bromolow'


def search_func(pattern):
    function_ids = subprocess.check_output([API_SEARCH, 'func', '-p', PLATFORM, pattern])
    found = 'id' == function_ids.splitlines()[0].split()[0]
    if found:
        return function_ids

    for candidates in function_ids.splitlines():
        id, func = candidates.split()
        if func == pattern:
            return search_func(id)

    error_message = '{} not found'.format(pattern)
    raise NameError(error_message)


def get_function_attribute(pattern, attributes):
    single_value_attr = frozenset(['id', 'name', 'demangled', 'visibility', 'platform', 'tag', 'file', 'line'])
    multi_value_attr = frozenset(['library', 'call', 'called'])
    valid_attrs = single_value_attr.union(multi_value_attr)

    attribute_set = frozenset(attributes)
    if not attribute_set.issubset(valid_attrs):
        error_message = '{} are not valid attributes'.format(','.join(attribute_set.difference(valid_attrs)))
        raise KeyError(error_message)

    data = search_func(pattern)
    result = {}
    lines = data.splitlines()
    key_lines = []
    for i, line in enumerate(lines):
        if not line.startswith(' '):
            key_lines.append(i)

    for i, key_line in enumerate(key_lines):
        splitted_line = lines[key_line].split(None, 1)
        if not splitted_line:
            continue

        key = splitted_line[0]
        key = key[:-1] if key.endswith(':') else key
        if key in attribute_set:
            if key in single_value_attr:
                result[key] = splitted_line[1]
            else:
                result[key] = []
                for p in range(key_line + 1, key_lines[i + 1]):
                    result[key].append(lines[p])
    return result


def get_caller_functions(func):
    callers = get_function_attribute(func, ['called'])
    for caller in callers['called']:
        id = caller.split()[0]
        caller_data = get_function_attribute(id, ['demangled', 'file'])
        print('\t'.join([
            caller_data['file'].split('/')[2],
            caller_data['demangled'],
            caller_data['file'],
        ]))


print('------{}------'.format(SYNOLogSet1))
get_caller_functions(SYNOLogSet1)
print('------{}------'.format(SLIBLogSet))
get_caller_functions(SLIBLogSet)
