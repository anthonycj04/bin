#!/usr/bin/env python
# -*- coding: utf-8 -*-

import socket
import random
from contextlib import closing
from datetime import datetime

LOG_NOTICE = 5
MESSAGE_LENGTH = 256
PORT = 514


def bsd_log():
    with closing(socket.socket(socket.AF_INET, socket.SOCK_DGRAM)) as sock:
        message = 'hihi'
        priority = LOG_NOTICE
        host = '10.12.4.59'

        now = datetime.now()
        timestamp = now.strftime('%b %%s %H:%M:%S') % ('%2d' % now.day)
        hostname = socket.gethostname()
        hostname += '_' + random.choice('0123456789')
        line = '<{}>{} {} {}'.format(
            priority, timestamp, hostname, message)
        if len(line) < MESSAGE_LENGTH:
            diff = (MESSAGE_LENGTH - len(line))
            line = ''.join([line] + ['PAD'] * (diff // 3) + ['PAD'[:diff % 3]])
        print(line)
        sock.sendto(line, (host, PORT))


def ietf_log():
    with closing(socket.socket(socket.AF_INET, socket.SOCK_DGRAM)) as sock:
        message = 'qqqqhihi'
        priority = LOG_NOTICE
        host = '10.12.4.59'

        now = datetime.now()
        timestamp = now.isoformat()
        hostname = socket.gethostname()
        hostname += '_' + random.choice('0123456789')
        line = '<{}>1 {} {} {} {} {} - {}'.format(priority, timestamp, hostname, 'app', 1, 'ID47', message)
        if len(line) < MESSAGE_LENGTH:
            diff = (MESSAGE_LENGTH - len(line))
            line = ''.join([line] + ['PAD'] * (diff // 3) + ['PAD'[:diff % 3]])
        print(line)
        sock.sendto(line, (host, PORT))

ietf_log()
