#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sqlite3
import argparse
import os
import time
import random
from faker import Faker


SYNOLOG_DIR = '/var/log/synolog'


class BaseXferLogClass():
    def __init__(self):
        self.faker = Faker()
        self.init()

    def get_db_path(self):
        raise NotImplementedError

    def gen_entries(self, num=1):
        entries = []
        for i in range(num):
            entries.append(self.gen_entry())
        return entries

    def gen_entry(self):
        # schema = id, time, ip, username, cmd, filesize, filename, isdir
        is_dir = self.faker.boolean()
        path = str(self.faker.file_path(random.randrange(5)))
        path = os.path.dirname(path) if is_dir else path
        filesize = 0 if is_dir else random.randrange(100000000)
        action = random.choice(self.folder_action) if is_dir else random.choice(self.file_action)

        return (None,
                int(time.time()),
                self.faker.ipv4(),
                'admin',
                action,
                filesize,
                path,
                is_dir)

    def write_log(self, num):
        conn = sqlite3.connect(self.get_db_path())
        entries = self.gen_entries(num)
        conn.cursor().executemany('INSERT INTO logs VALUES (?, ?, ?, ?, ?, ?, ?, ?)', entries)
        conn.commit()
        conn.close()


class BaseGeneralLogClass():
    def __init__(self):
        self.faker = Faker()

    def get_db_path(self):
        raise NotImplementedError

    def gen_entries(self, num=1):
        entries = []
        for i in range(num):
            entries.append(self.gen_entry())
        return entries

    def gen_entry(self):
        # schema = id, time, level, username, msg

        return (None,
                int(time.time()),
                'info',
                'admin',
                self.faker.sentence(nb_words=6))

    def write_log(self, num):
        conn = sqlite3.connect(self.get_db_path())
        entries = self.gen_entries(num)
        conn.cursor().executemany('INSERT INTO logs VALUES (?, ?, ?, ?, ?)', entries)
        conn.commit()
        conn.close()


class SYSLog(BaseGeneralLogClass):
    def get_db_path(self):
        return os.path.join(SYNOLOG_DIR, '.SYNOSYSDB')


class ShareFolderSyncLog(BaseGeneralLogClass):
    def get_db_path(self):
        return os.path.join(SYNOLOG_DIR, '.SYNONETBKPDB')


class FileStationLog(BaseXferLogClass):
    def init(self):
        self.file_action = ['move', 'delete', 'copy', 'upload']
        self.folder_action = ['move', 'delete', 'copy', 'mkdir']

    def get_db_path(self):
        return os.path.join(SYNOLOG_DIR, '.DSMFMXFERDB')


class SambaLog(BaseXferLogClass):
    def init(self):
        self.file_action = ['move', 'delete', 'create', 'read', 'write']
        self.folder_action = ['move', 'delete', 'create']

    def get_db_path(self):
        return os.path.join(SYNOLOG_DIR, '.SMBXFERDB')


class FTPLog(BaseXferLogClass):
    def init(self):
        self.file_action = ['put', 'get', 'delete']
        self.folder_action = ['create folder', 'delete folder']

    def get_db_path(self):
        return os.path.join(SYNOLOG_DIR, '.FTPXFERDB')


class TFTPLog(BaseXferLogClass):
    def init(self):
        self.file_action = ['put', 'get', 'delete']
        self.folder_action = ['create folder', 'delete folder']

    def get_db_path(self):
        return os.path.join(SYNOLOG_DIR, '.TFTPXFERDB')


class WEBDAVLog(BaseXferLogClass):
    def init(self):
        self.file_action = ['upload', 'download', 'delete', 'copy']
        self.folder_action = ['delete', 'upload']

    def get_db_path(self):
        return os.path.join(SYNOLOG_DIR, '.WEBDAVXFERDB')


def insert_entries(args):
    class_mapping = {
        'smb': SambaLog,
        'fs': FileStationLog,
        'ftp': FTPLog,
        'tftp': TFTPLog,
        'webdav': WEBDAVLog,
        'sys': SYSLog,
        'sync': ShareFolderSyncLog,
    }

    if args.db not in class_mapping:
        print('invalid db {}, choose one from {}'.format(args.db, class_mapping.keys()))
        exit(1)

    log = class_mapping[args.db]()
    log.write_log(args.number)


def main():
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-n', '--number', type=int, default=1,
                            help='num of entry to add')
    arg_parser.add_argument('db', help='database to insert to')

    args = arg_parser.parse_args()
    insert_entries(args)


if __name__ == "__main__":
    main()
