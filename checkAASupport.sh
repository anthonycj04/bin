#!/bin/sh

pkg="$1"
if [ -z "$pkg" ]; then
    echo "Usage: $0 spkfile"
    exit 1
fi

tar xOf "$pkg" INFO | grep -q support_aaprofile
[ $? -eq 0 ] && echo yes || echo no
