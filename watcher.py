#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
import os
from shutil import copyfile
from watchdog.observers import Observer
from watchdog.events import RegexMatchingEventHandler


class Handler(RegexMatchingEventHandler):
    @staticmethod
    def on_any_event(event):
        if event.event_type == 'modified':
            target_path = os.path.join('/synosrc/curr/ds.avoton', os.path.basename(event.src_path))
            print('{}: modified {}, copying to {}'.format(time.ctime(), event.src_path, target_path))
            copyfile(event.src_path, target_path)


def main():
    event_handler = Handler(regexes=['/root/bin/genLogs.py'])
    path = '/root/bin'
    observer = Observer()

    observer.schedule(event_handler, path)
    observer.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()

if __name__ == "__main__":
    main()
